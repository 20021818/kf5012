close all
clear
clc
%% start video reader and player
video_reader = vision.VideoFileReader('ball.mp4',...
    'VideoOutputDataType','double');
video_player = vision.DeployableVideoPlayer;

%% init tracker
image = step(video_reader);
figure
imshow(image)
select_box = imrect;
wait(select_box);
[~,center_loc] = getCenterloc(select_box);

motion_model = 'ConstantVelocity';
initial_loc = center_loc;
initial_err = 10 * ones(1,2);
motion_noise = [20 20];
measurement_noise = 200;
kalman_filter = configureKalmanFilter(...
                        motion_model,...
                        initial_loc,...
                        initial_err,...
                        motion_noise,...
                        measurement_noise);

%% track moving object
i = 1;
while ~isDone(video_reader)
    image = step(video_reader);
    tracked_loc = predict(kalman_filter);
    output = insertShape(image,'circle',[tracked_loc 20],...
        'Color','green','LineWidth',5);
    
    % find object 
    [~,detected_loc] = detectObject(image,5000);
    if (~isempty(detected_loc))
        % if object found correct kalman filter
        tracked_loc = correct(kalman_filter,detected_loc);
        output = insertShape(output,'circle',[tracked_loc 5],...
        'Color','blue','LineWidth',5);
    end
    
    step(video_player,output);
    
    pos(i,:) = tracked_loc;
    i = i+1;
    pause(0.1);

end

figure;
imshow(image)
hold on
plot(pos(:,1),pos(:,2),'rx-','LineWidth',2)


release(video_player);
release(video_reader);