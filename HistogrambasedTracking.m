close all
clear
clc
%% start video reader and player
video_reader = vision.VideoFileReader('ball.mp4',...
    'VideoOutputDataType','double');
video_player = vision.DeployableVideoPlayer;

%% create histogrambasedtracker object
obj_tracker = vision.HistogramBasedTracker;

%% init tracker
image = step(video_reader);
figure
imshow(image)
select_box = imrect;
wait(select_box)
box_location = getCenterloc(select_box);
image_ycbcr = rgb2ycbcr(image);
initializeObject(obj_tracker,image_ycbcr(:,:,2),box_location);

%% track moving object
i = 1;
while ~isDone(video_reader)
    image = step(video_reader);
    image_ycbcr = rgb2ycbcr(image);
    [bbox,~,score(i)] = step(obj_tracker,image_ycbcr(:,:,2));
    
    if score(i) > 0.5
        output = insertShape(image,'Rectangle',bbox);
    else
        % find object
        box_location = detectObject(image,5000);
        if (~isempty(box_location))
            % if object found reinit and track again
            initializeSearchWindow(obj_tracker,box_location)
            [bbox,~,score(i)] = step(obj_tracker,image_ycbcr(:,:,2));
            output = insertShape(image,'Rectangle',bbox);
        else
            % if object not found output normal video frames
            output = image;
        end
    end
    
    step(video_player,output);       
    i = i+1;
end

figure;
plot(score)
xlabel('Frame #')
ylabel('Confidence Score (0,1)')

release(video_player);
release(video_reader);