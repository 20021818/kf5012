function [box_location,centroid_location] = getCenterloc(hrect)
box = getPosition(hrect);

box_location = round(box);

centroid_location(1) = box(1) + (box(3)/2);
centroid_location(2) = box(2) + (box(4)/2);

end