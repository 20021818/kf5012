function [box_location,centroid_location] = detectObject(video_frame,min_area)
persistent blob  
% create blobananlysis object
if isempty(blob)
blob = vision.BlobAnalysis;
blob.AreaOutputPort = false;
% excluse object touching border & objects with less than min area pixels
blob.ExcludeBorderBlobs = true;
blob.MinimumBlobArea = min_area;
end
% apply image processing
hsv_image = rgb2hsv(video_frame);
image_hue = hsv_image(:,:,1);
image = image_hue >= 0.1 & image_hue < 0.15;
% remove disturbances
image = imopen(image,strel('disk', 7));
% get properties of objects with more than minarea pixels
[centroid_location,box_location] = step(blob, image);
box_location = round(box_location);


