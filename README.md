# Automated object tracking in digital videos

This is the MATLAB code for the KF5012: Intelligent systems project.

## Installation

Copy the entire contents of the repository into a folder and open the folder in MATLAB.

## Histogram-based tracker
Run ```HistogrambasedTracking.m```

Select a tracking area using the provided utility and double click inside the selected area to initiate the tracking.

## Kalman Filter tracker
Run ```KalmanFilterTracking.m```

Select a tracking area using the provided utility and double click inside the selected area to initiate the tracking.

## License
[MIT](https://choosealicense.com/licenses/mit/)